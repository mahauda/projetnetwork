package universite.angers.master.info.network.communicator;

/**
 * Interface qui permet de savoir si un traitement est fini ou non
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public interface Finishable {
	
	/**
	 * Finir le traitement
	 * @return
	 */
	public boolean finish();
	
	/**
	 * Le traitement à faire est-il finit ?
	 * @return
	 */
	public boolean isFinish();
}
