package universite.angers.master.info.network.client;

import java.util.concurrent.locks.ReadWriteLock;
import org.apache.log4j.Logger;
import universite.angers.master.info.api.converter.Convertable;
import universite.angers.master.info.network.service.Commandable;
import universite.angers.master.info.network.service.Service;

/**
 * Client qui permet d'envoyer des requetes au serveur
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class ClientService<T> extends Service<T> {

	private static final Logger LOG = Logger.getLogger(ClientService.class);
	
	public ClientService(Commandable<T> requestDefault, Commandable<T> notificationDefault, 
			Convertable<String, T> commandName, ReadWriteLock lock) {
		super(requestDefault, notificationDefault, commandName, lock);
	}
	
	@Override
	public T talk() {
		
		//Le client pose le verrou sur la requete. Il ne peut traiter qu'une seule requete à la
		//fois afin de synchroniser les échanges. Envoyer des requetes "asynchrones" restent compliqué
		//à gérer
		this.lock.writeLock().lock();
		
		//Appel bloquant tant que la liste des requetes est vide
		LOG.debug("Wait Request ...");
		this.requestCurrent = this.requests.talk();
		LOG.debug("Request : " + this.requestCurrent);
		
		if(this.requestCurrent == null) {
			this.lock.writeLock().unlock();
			return null;
		}
		
		//On émet la commande
		return this.requestCurrent.receive(null);
	}
	
	@Override
	public boolean listen(T response) {
		LOG.debug("Response : " + response);
		
		//Si la réponse est null dans ce cas on relache le verrou et on effectue une requete par défaut
		//Ex : message d'erreurs, etc.
		if(response == null || this.requestCurrent == null) {
			boolean listen = this.requestDefault.send(response);
			LOG.debug("Listen request default : " + listen);
			
			this.lock.writeLock().unlock();
			
			return listen;
		}
		
		boolean listen = this.requestCurrent.send(response);
		LOG.debug("Listen request current : " + listen);
		
		this.lock.writeLock().unlock();
		
		return listen;
	}

	@Override
	public boolean isLimited(int request) {
		return false;
	}
	
	@Override
	public boolean close() {
		boolean closeRequest = this.requests.close();
		boolean closeNotify = this.notifications.close();
		return closeRequest && closeNotify;
	}

	@Override
	public boolean isClose() {
		return this.requests.isClose() || this.notifications.isClose();
	}

	@Override
	public boolean finish() {
		boolean requestFinish = this.requests.finish();
		boolean notifyRequest = this.notifications.finish();
		return requestFinish && notifyRequest;
	}

	@Override
	public boolean isFinish() {
		//C'est finis si toutes les requetes sont traités
		return this.requests.isFinish();
	}
	
	@Override
	public int hashCode() {
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ClientService [requests=" + requests + ", requestDefault=" + requestDefault + ", requestCurrent="
				+ requestCurrent + ", notifications=" + notifications + ", notificationDefault=" + notificationDefault
				+ ", commandName=" + commandName + ", lock=" + lock + "]";
	}
}
