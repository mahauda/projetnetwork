package universite.angers.master.info.network.communicator;

/**
 * Interface qui permet d'envoyer un message par le sujet
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public interface Observer<T> {
	
	/**
	 * Transmettre un message à tous les observateurs à travers un visiteur
	 * @param o le sujet
	 * @param arg le message à communiquer aux observateurs qui écoutent
	 */
	public boolean notify(T message);
	
	/**
	 * Vérifie si le message est une notification
	 * @param message
	 * @return
	 */
	public boolean isNotify(T message);
}
