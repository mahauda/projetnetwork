package universite.angers.master.info.network.server;

import universite.angers.master.info.network.communicator.Communicator;

/**
 * Interface qui permet de notifier tous les observateurs
 */
public interface Observable<T, K> {

	/**
	 * Notifier une communication d'un sujet
	 * @param subject
	 * @param com
	 * @param arg
	 * @return
	 */
	public boolean notifyObserver(String subject, Communicator<T, K> com, Object arg);
	
	/**
	 * Notifier tous les communications d'un sujet
	 * @param subject
	 * @param arg
	 * @return
	 */
	public boolean notifyAllObservers(String subject, Object arg);
}
