package universite.angers.master.info.network.server.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

public class Account {

	private String id;
	private User user;
	private Collection<Game> gamesWon;
	private Collection<Game> gamesLost;
	
	public Account() {
		this(new User(), new ArrayList<>(), new ArrayList<>());
	}
	
	public Account(User user, Collection<Game> gamesWon, Collection<Game> gamesLost) {
		this.id = UUID.randomUUID().toString();
		this.user = user;
		this.gamesWon = gamesWon;
		this.gamesLost = gamesLost;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the gamesWon
	 */
	public Collection<Game> getGamesWon() {
		return gamesWon;
	}

	/**
	 * @param gamesWon the gamesWon to set
	 */
	public void setGamesWon(Collection<Game> gamesWon) {
		this.gamesWon = gamesWon;
	}

	/**
	 * @return the gamesLost
	 */
	public Collection<Game> getGamesLost() {
		return gamesLost;
	}

	/**
	 * @param gamesLost the gamesLost to set
	 */
	public void setGamesLost(Collection<Game> gamesLost) {
		this.gamesLost = gamesLost;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((gamesLost == null) ? 0 : gamesLost.hashCode());
		result = prime * result + ((gamesWon == null) ? 0 : gamesWon.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (gamesLost == null) {
			if (other.gamesLost != null)
				return false;
		} else if (!gamesLost.equals(other.gamesLost))
			return false;
		if (gamesWon == null) {
			if (other.gamesWon != null)
				return false;
		} else if (!gamesWon.equals(other.gamesWon))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Accout [id=" + id + ", user=" + user + ", gamesWon=" + gamesWon + ", gamesLost=" + gamesLost + "]";
	}
}
