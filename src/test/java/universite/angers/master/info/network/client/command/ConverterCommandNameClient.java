package universite.angers.master.info.network.client.command;

import universite.angers.master.info.api.converter.Convertable;

public class ConverterCommandNameClient implements Convertable<String, String> {

	@Override
	public String convert(String message) {
		return message.split(";")[0];
	}
}
