package universite.angers.master.info.network.client;

import universite.angers.master.info.api.translater.TranslaterStringToString;
import universite.angers.master.info.network.client.command.ConverterCommandNameClient;
import universite.angers.master.info.network.client.command.DateObject;
import universite.angers.master.info.network.client.command.notify.ClientNotifyClose;
import universite.angers.master.info.network.client.command.notify.ClientNotifyEcho;
import universite.angers.master.info.network.client.command.notify.ClientNotifyHello;
import universite.angers.master.info.network.client.command.notify.ClientNotifyNew;
import universite.angers.master.info.network.client.command.request.ClientRequestDay;
import universite.angers.master.info.network.client.command.request.ClientRequestHour;
import universite.angers.master.info.network.client.command.request.ClientRequestMonth;
import universite.angers.master.info.network.client.command.request.ClientRequestYear;
import universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock;
import universite.angers.master.info.network.server.Subject;

/**
 * Classe qui permet de tester la demande de la date du jour au serveur par deux clients
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 26/01/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class MainClient {

	public static void main(String[] args) {
		String host = "127.0.0.1";
		int port = 2345;
		
		DateObject date1 = new DateObject("", "", "", "");
		DateObject date2 = new DateObject("", "", "", "");
		
		ClientService<String> service1 = new ClientService<>(null, null, new ConverterCommandNameClient(), new ReentrantWithoutReadWriteLock(true));
		ClientService<String> service2 = new ClientService<>(null, null, new ConverterCommandNameClient(), new ReentrantWithoutReadWriteLock(true));
		ClientService<String> service3 = new ClientService<>(null, null, new ConverterCommandNameClient(), new ReentrantWithoutReadWriteLock(true));
		
		//SERVICE1
		
		//On enregistre les notifications
		service1.registerNotification(Subject.NOTIFY_ECHO.getName(), new ClientNotifyEcho());
        service1.registerNotification(Subject.NOTIFY_HELLO.getName(), new ClientNotifyHello());
        service1.registerNotification(Subject.NOTIFY_NEW.getName(), new ClientNotifyNew());
        service1.registerNotification(Subject.NOTIFY_CLOSE.getName(), new ClientNotifyClose());
		
		//On enregistre les requete à envoyer aux serveurs
        service1.registerRequest("DAY", new ClientRequestDay(date1));
        service1.registerRequest("MONTH", new ClientRequestMonth(date1));
		
        //SERVICE2
        
        //On enregistre les notifications
        service2.registerNotification(Subject.NOTIFY_ECHO.getName(), new ClientNotifyEcho());
        service2.registerNotification(Subject.NOTIFY_HELLO.getName(), new ClientNotifyHello());
        service2.registerNotification(Subject.NOTIFY_NEW.getName(), new ClientNotifyNew());
        service2.registerNotification(Subject.NOTIFY_CLOSE.getName(), new ClientNotifyClose());
        
		//On enregistre les requete à envoyer aux serveurs
        service2.registerRequest("YEAR", new ClientRequestYear(date1));
        service2.registerRequest("TIME", new ClientRequestHour(date1));
        
        //SERVICE3
        
        //On enregistre les notifications
        service3.registerNotification(Subject.NOTIFY_ECHO.getName(), new ClientNotifyEcho());
        service3.registerNotification(Subject.NOTIFY_HELLO.getName(), new ClientNotifyHello());
        service3.registerNotification(Subject.NOTIFY_NEW.getName(), new ClientNotifyNew());
        service3.registerNotification(Subject.NOTIFY_CLOSE.getName(), new ClientNotifyClose());
        
        Client<String> client1 = new Client<>(host, port, service1, new TranslaterStringToString(), new TranslaterStringToString());
        Client<String> client2 = new Client<>(host, port, service2, new TranslaterStringToString(), new TranslaterStringToString());
        Client<String> client3 = new Client<>(host, port, service3, new TranslaterStringToString(), new TranslaterStringToString());
        
        System.out.println("Date 1 avant : " + date1);
        System.out.println("Date 2 avant : " + date2);
 
		client1.open();
		client2.open();
		
	    client1.waitFinish();
	    client2.waitFinish();

	    client2.close();
	    client3.open();
	    
	    waitClient(5000);
	    
	    client1.close();
	    client3.close();
	    
	    System.out.println("Date 1 après : " + date1);
	    System.out.println("Date 2 après : " + date2);
	}
	

	private static void waitClient(long time) {
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}

/**
 * 23:01:18,701 DEBUG main:Service:<init>:69 - Requests BlockingMap [map={}, finish=false, open=false]
23:01:18,703 DEBUG main:Service:<init>:72 - Notifications BlockingMap [map={}, finish=false, open=false]
23:01:18,703 DEBUG main:Service:<init>:75 - Requete par défaut : null
23:01:18,703 DEBUG main:Service:<init>:78 - Notification par défaut : null
23:01:18,705 DEBUG main:Service:<init>:81 - Command name universite.angers.master.info.network.client.command.ConverterCommandNameClient@9807454
23:01:18,705 DEBUG main:Service:<init>:84 - Verrou universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock@3d494fbf
23:01:18,705 DEBUG main:Service:<init>:69 - Requests BlockingMap [map={}, finish=false, open=false]
23:01:18,705 DEBUG main:Service:<init>:72 - Notifications BlockingMap [map={}, finish=false, open=false]
23:01:18,705 DEBUG main:Service:<init>:75 - Requete par défaut : null
23:01:18,705 DEBUG main:Service:<init>:78 - Notification par défaut : null
23:01:18,705 DEBUG main:Service:<init>:81 - Command name universite.angers.master.info.network.client.command.ConverterCommandNameClient@1ddc4ec2
23:01:18,705 DEBUG main:Service:<init>:84 - Verrou universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock@133314b
23:01:18,705 DEBUG main:Service:<init>:69 - Requests BlockingMap [map={}, finish=false, open=false]
23:01:18,705 DEBUG main:Service:<init>:72 - Notifications BlockingMap [map={}, finish=false, open=false]
23:01:18,705 DEBUG main:Service:<init>:75 - Requete par défaut : null
23:01:18,705 DEBUG main:Service:<init>:78 - Notification par défaut : null
23:01:18,707 DEBUG main:Service:<init>:81 - Command name universite.angers.master.info.network.client.command.ConverterCommandNameClient@4edde6e5
23:01:18,707 DEBUG main:Service:<init>:84 - Verrou universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock@70177ecd
23:01:18,708 DEBUG main:Service:registerNotification:157 - Notification : HELLO_CLIENT
23:01:18,708 DEBUG main:Service:registerNotification:158 - Command : universite.angers.master.info.network.client.command.notify.HelloClientNotify@1e80bfe8
23:01:18,709 DEBUG main:Service:registerNotification:165 - Notification ajoutée
23:01:18,710 DEBUG main:Service:registerNotification:157 - Notification : NEW_CLIENT
23:01:18,710 DEBUG main:Service:registerNotification:158 - Command : universite.angers.master.info.network.client.command.notify.NewClientNotify@66a29884
23:01:18,710 DEBUG main:Service:registerNotification:165 - Notification ajoutée
23:01:18,711 DEBUG main:Service:registerNotification:157 - Notification : CLOSE_CLIENT
23:01:18,711 DEBUG main:Service:registerNotification:158 - Command : universite.angers.master.info.network.client.command.notify.CloseClientNotify@4769b07b
23:01:18,711 DEBUG main:Service:registerNotification:165 - Notification ajoutée
23:01:18,711 DEBUG main:Service:registerRequest:134 - Request : DAY
23:01:18,711 DEBUG main:Service:registerRequest:135 - Command : universite.angers.master.info.network.client.command.request.DateObjectDayCommand@cc34f4d
23:01:18,711 DEBUG main:Service:registerRequest:142 - Requete ajoutée
23:01:18,712 DEBUG main:Service:registerRequest:134 - Request : MONTH
23:01:18,712 DEBUG main:Service:registerRequest:135 - Command : universite.angers.master.info.network.client.command.request.DateObjectMonthCommand@17a7cec2
23:01:18,712 DEBUG main:Service:registerRequest:142 - Requete ajoutée
23:01:18,712 DEBUG main:Service:registerNotification:157 - Notification : HELLO_CLIENT
23:01:18,712 DEBUG main:Service:registerNotification:158 - Command : universite.angers.master.info.network.client.command.notify.HelloClientNotify@65b3120a
23:01:18,713 DEBUG main:Service:registerNotification:165 - Notification ajoutée
23:01:18,713 DEBUG main:Service:registerNotification:157 - Notification : NEW_CLIENT
23:01:18,713 DEBUG main:Service:registerNotification:158 - Command : universite.angers.master.info.network.client.command.notify.NewClientNotify@6f539caf
23:01:18,713 DEBUG main:Service:registerNotification:165 - Notification ajoutée
23:01:18,713 DEBUG main:Service:registerNotification:157 - Notification : CLOSE_CLIENT
23:01:18,713 DEBUG main:Service:registerNotification:158 - Command : universite.angers.master.info.network.client.command.notify.CloseClientNotify@79fc0f2f
23:01:18,713 DEBUG main:Service:registerNotification:165 - Notification ajoutée
23:01:18,713 DEBUG main:Service:registerRequest:134 - Request : YEAR
23:01:18,714 DEBUG main:Service:registerRequest:135 - Command : universite.angers.master.info.network.client.command.request.DateObjectYearCommand@50040f0c
23:01:18,714 DEBUG main:Service:registerRequest:142 - Requete ajoutée
23:01:18,714 DEBUG main:Service:registerRequest:134 - Request : TIME
23:01:18,714 DEBUG main:Service:registerRequest:135 - Command : universite.angers.master.info.network.client.command.request.DateObjectHourCommand@2dda6444
23:01:18,714 DEBUG main:Service:registerRequest:142 - Requete ajoutée
23:01:18,714 DEBUG main:Service:registerNotification:157 - Notification : HELLO_CLIENT
23:01:18,714 DEBUG main:Service:registerNotification:158 - Command : universite.angers.master.info.network.client.command.notify.HelloClientNotify@5e9f23b4
23:01:18,714 DEBUG main:Service:registerNotification:165 - Notification ajoutée
23:01:18,715 DEBUG main:Service:registerNotification:157 - Notification : NEW_CLIENT
23:01:18,715 DEBUG main:Service:registerNotification:158 - Command : universite.angers.master.info.network.client.command.notify.NewClientNotify@4783da3f
23:01:18,715 DEBUG main:Service:registerNotification:165 - Notification ajoutée
23:01:18,715 DEBUG main:Service:registerNotification:157 - Notification : CLOSE_CLIENT
23:01:18,715 DEBUG main:Service:registerNotification:158 - Command : universite.angers.master.info.network.client.command.notify.CloseClientNotify@378fd1ac
23:01:18,715 DEBUG main:Service:registerNotification:165 - Notification ajoutée
23:01:18,721 DEBUG main:Client:<init>:83 - Host : 127.0.0.1
23:01:18,721 DEBUG main:Client:<init>:86 - Port : 2345
23:01:18,725 DEBUG main:Client:<init>:89 - Client socket : Socket[addr=/127.0.0.1,port=2345,localport=35754]
23:01:18,725 DEBUG main:Client:<init>:92 - Client service : ClientService [requests=BlockingMap [map={MONTH=universite.angers.master.info.network.client.command.request.DateObjectMonthCommand@17a7cec2, DAY=universite.angers.master.info.network.client.command.request.DateObjectDayCommand@cc34f4d}, finish=false, open=false], requestDefault=null, requestCurrent=null, notifications=BlockingMap [map={HELLO_CLIENT=universite.angers.master.info.network.client.command.notify.HelloClientNotify@1e80bfe8, NEW_CLIENT=universite.angers.master.info.network.client.command.notify.NewClientNotify@66a29884, CLOSE_CLIENT=universite.angers.master.info.network.client.command.notify.CloseClientNotify@4769b07b}, finish=false, open=false], notificationDefault=null, commandName=universite.angers.master.info.network.client.command.ConverterCommandNameClient@9807454, lock=universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock@3d494fbf]
23:01:18,725 DEBUG main:Client:<init>:95 - Traducteur client service/serveur : universite.angers.master.info.api.translater.TranslaterStringToString@7106e68e
23:01:18,725 DEBUG main:Client:<init>:98 - Traducteur serveur/client service: universite.angers.master.info.api.translater.TranslaterStringToString@7eda2dbb
23:01:18,725 DEBUG main:Client:<init>:101 - Thread client : Thread[Client0,5,main]
23:01:18,725 DEBUG main:Client:<init>:83 - Host : 127.0.0.1
23:01:18,725 DEBUG main:Client:<init>:86 - Port : 2345
23:01:18,725 DEBUG main:Client:<init>:89 - Client socket : Socket[addr=/127.0.0.1,port=2345,localport=35756]
23:01:18,725 DEBUG main:Client:<init>:92 - Client service : ClientService [requests=BlockingMap [map={YEAR=universite.angers.master.info.network.client.command.request.DateObjectYearCommand@50040f0c, TIME=universite.angers.master.info.network.client.command.request.DateObjectHourCommand@2dda6444}, finish=false, open=false], requestDefault=null, requestCurrent=null, notifications=BlockingMap [map={HELLO_CLIENT=universite.angers.master.info.network.client.command.notify.HelloClientNotify@65b3120a, NEW_CLIENT=universite.angers.master.info.network.client.command.notify.NewClientNotify@6f539caf, CLOSE_CLIENT=universite.angers.master.info.network.client.command.notify.CloseClientNotify@79fc0f2f}, finish=false, open=false], notificationDefault=null, commandName=universite.angers.master.info.network.client.command.ConverterCommandNameClient@1ddc4ec2, lock=universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock@133314b]
23:01:18,725 DEBUG main:Client:<init>:95 - Traducteur client service/serveur : universite.angers.master.info.api.translater.TranslaterStringToString@6576fe71
23:01:18,725 DEBUG main:Client:<init>:98 - Traducteur serveur/client service: universite.angers.master.info.api.translater.TranslaterStringToString@76fb509a
23:01:18,726 DEBUG main:Client:<init>:101 - Thread client : Thread[Client1,5,main]
23:01:18,726 DEBUG main:Client:<init>:83 - Host : 127.0.0.1
23:01:18,726 DEBUG main:Client:<init>:86 - Port : 2345
23:01:18,726 DEBUG main:Client:<init>:89 - Client socket : Socket[addr=/127.0.0.1,port=2345,localport=35758]
23:01:18,726 DEBUG main:Client:<init>:92 - Client service : ClientService [requests=BlockingMap [map={}, finish=false, open=false], requestDefault=null, requestCurrent=null, notifications=BlockingMap [map={HELLO_CLIENT=universite.angers.master.info.network.client.command.notify.HelloClientNotify@5e9f23b4, NEW_CLIENT=universite.angers.master.info.network.client.command.notify.NewClientNotify@4783da3f, CLOSE_CLIENT=universite.angers.master.info.network.client.command.notify.CloseClientNotify@378fd1ac}, finish=false, open=false], notificationDefault=null, commandName=universite.angers.master.info.network.client.command.ConverterCommandNameClient@4edde6e5, lock=universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock@70177ecd]
23:01:18,726 DEBUG main:Client:<init>:95 - Traducteur client service/serveur : universite.angers.master.info.api.translater.TranslaterStringToString@300ffa5d
23:01:18,726 DEBUG main:Client:<init>:98 - Traducteur serveur/client service: universite.angers.master.info.api.translater.TranslaterStringToString@1f17ae12
23:01:18,726 DEBUG main:Client:<init>:101 - Thread client : Thread[Client2,5,main]
Date 1 avant : DateObject [day=, month=, year=, hour=]
Date 2 avant : DateObject [day=, month=, year=, hour=]
23:01:18,726 DEBUG main:Client:open:187 - Open client service : true
23:01:18,727 DEBUG main:Client:open:187 - Open client service : true
23:01:18,727 DEBUG Client0:ReaderSocket:<init>:29 - Socket : null
23:01:18,727 DEBUG Client1:ReaderSocket:<init>:29 - Socket : null
23:01:18,728 DEBUG Client1:Client:run:113 - Reader socket : ReaderSocketStream [socket=null]
23:01:18,728 DEBUG Client1:Client:run:117 - Writer socket : WriterSocketStream [socket=null]
23:01:18,727 DEBUG Client0:Client:run:113 - Reader socket : ReaderSocketStream [socket=null]
23:01:18,728 DEBUG Client0:Client:run:117 - Writer socket : WriterSocketStream [socket=null]
23:01:18,730 DEBUG Client1:Client:run:121 - Service socket : CommunicatorSocket [socket=Socket[addr=/127.0.0.1,port=2345,localport=35756], reader=ReaderSocketStream [socket=Socket[addr=/127.0.0.1,port=2345,localport=35756]], writer=WriterSocketStream [socket=Socket[addr=/127.0.0.1,port=2345,localport=35756]]]
23:01:18,730 DEBUG Client0:Client:run:121 - Service socket : CommunicatorSocket [socket=Socket[addr=/127.0.0.1,port=2345,localport=35754], reader=ReaderSocketStream [socket=Socket[addr=/127.0.0.1,port=2345,localport=35754]], writer=WriterSocketStream [socket=Socket[addr=/127.0.0.1,port=2345,localport=35754]]]
23:01:18,731 DEBUG Client1:Communicator:<init>:54 - Entité A : ClientService [requests=BlockingMap [map={YEAR=universite.angers.master.info.network.client.command.request.DateObjectYearCommand@50040f0c, TIME=universite.angers.master.info.network.client.command.request.DateObjectHourCommand@2dda6444}, finish=false, open=true], requestDefault=null, requestCurrent=null, notifications=BlockingMap [map={HELLO_CLIENT=universite.angers.master.info.network.client.command.notify.HelloClientNotify@65b3120a, NEW_CLIENT=universite.angers.master.info.network.client.command.notify.NewClientNotify@6f539caf, CLOSE_CLIENT=universite.angers.master.info.network.client.command.notify.CloseClientNotify@79fc0f2f}, finish=false, open=true], notificationDefault=null, commandName=universite.angers.master.info.network.client.command.ConverterCommandNameClient@1ddc4ec2, lock=universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock@133314b]
23:01:18,731 DEBUG Client1:Communicator:<init>:57 - Entité B : CommunicatorSocket [socket=Socket[addr=/127.0.0.1,port=2345,localport=35756], reader=ReaderSocketStream [socket=Socket[addr=/127.0.0.1,port=2345,localport=35756]], writer=WriterSocketStream [socket=Socket[addr=/127.0.0.1,port=2345,localport=35756]]]
23:01:18,731 DEBUG Client1:Communicator:<init>:60 - Traducteur : universite.angers.master.info.api.translater.TranslaterStringToString@6576fe71
23:01:18,731 DEBUG Client0:Communicator:<init>:54 - Entité A : ClientService [requests=BlockingMap [map={MONTH=universite.angers.master.info.network.client.command.request.DateObjectMonthCommand@17a7cec2, DAY=universite.angers.master.info.network.client.command.request.DateObjectDayCommand@cc34f4d}, finish=false, open=true], requestDefault=null, requestCurrent=null, notifications=BlockingMap [map={HELLO_CLIENT=universite.angers.master.info.network.client.command.notify.HelloClientNotify@1e80bfe8, NEW_CLIENT=universite.angers.master.info.network.client.command.notify.NewClientNotify@66a29884, CLOSE_CLIENT=universite.angers.master.info.network.client.command.notify.CloseClientNotify@4769b07b}, finish=false, open=true], notificationDefault=null, commandName=universite.angers.master.info.network.client.command.ConverterCommandNameClient@9807454, lock=universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock@3d494fbf]
23:01:18,731 DEBUG Client0:Communicator:<init>:57 - Entité B : CommunicatorSocket [socket=Socket[addr=/127.0.0.1,port=2345,localport=35754], reader=ReaderSocketStream [socket=Socket[addr=/127.0.0.1,port=2345,localport=35754]], writer=WriterSocketStream [socket=Socket[addr=/127.0.0.1,port=2345,localport=35754]]]
23:01:18,731 DEBUG Client0:Communicator:<init>:60 - Traducteur : universite.angers.master.info.api.translater.TranslaterStringToString@7106e68e
23:01:18,731 DEBUG Client0:Communicator:<init>:63 - Echanges : 0
23:01:18,731 DEBUG Client1:Communicator:<init>:63 - Echanges : 0
23:01:18,731 DEBUG Client0:Communicator:<init>:66 - Thread communication : Thread[Communication0,5,main]
23:01:18,731 DEBUG Client1:Communicator:<init>:66 - Thread communication : Thread[Communication0,5,main]
23:01:18,732 DEBUG Client0:Client:run:126 - Communication entre client et socket : Communicator [entityA=ClientService [requests=BlockingMap [map={MONTH=universite.angers.master.info.network.client.command.request.DateObjectMonthCommand@17a7cec2, DAY=universite.angers.master.info.network.client.command.request.DateObjectDayCommand@cc34f4d}, finish=false, open=true], requestDefault=null, requestCurrent=null, notifications=BlockingMap [map={HELLO_CLIENT=universite.angers.master.info.network.client.command.notify.HelloClientNotify@1e80bfe8, NEW_CLIENT=universite.angers.master.info.network.client.command.notify.NewClientNotify@66a29884, CLOSE_CLIENT=universite.angers.master.info.network.client.command.notify.CloseClientNotify@4769b07b}, finish=false, open=true], notificationDefault=null, commandName=universite.angers.master.info.network.client.command.ConverterCommandNameClient@9807454, lock=universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock@3d494fbf], entityB=CommunicatorSocket [socket=Socket[addr=/127.0.0.1,port=2345,localport=35754], reader=ReaderSocketStream [socket=Socket[addr=/127.0.0.1,port=2345,localport=35754]], writer=WriterSocketStream [socket=Socket[addr=/127.0.0.1,port=2345,localport=35754]]], translator=universite.angers.master.info.api.translater.TranslaterStringToString@7106e68e, request=0, threadCommunication=Thread[Communication0,5,main]]
23:01:18,732 DEBUG Client1:Client:run:126 - Communication entre client et socket : Communicator [entityA=ClientService [requests=BlockingMap [map={YEAR=universite.angers.master.info.network.client.command.request.DateObjectYearCommand@50040f0c, TIME=universite.angers.master.info.network.client.command.request.DateObjectHourCommand@2dda6444}, finish=false, open=true], requestDefault=null, requestCurrent=null, notifications=BlockingMap [map={HELLO_CLIENT=universite.angers.master.info.network.client.command.notify.HelloClientNotify@65b3120a, NEW_CLIENT=universite.angers.master.info.network.client.command.notify.NewClientNotify@6f539caf, CLOSE_CLIENT=universite.angers.master.info.network.client.command.notify.CloseClientNotify@79fc0f2f}, finish=false, open=true], notificationDefault=null, commandName=universite.angers.master.info.network.client.command.ConverterCommandNameClient@1ddc4ec2, lock=universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock@133314b], entityB=CommunicatorSocket [socket=Socket[addr=/127.0.0.1,port=2345,localport=35756], reader=ReaderSocketStream [socket=Socket[addr=/127.0.0.1,port=2345,localport=35756]], writer=WriterSocketStream [socket=Socket[addr=/127.0.0.1,port=2345,localport=35756]]], translator=universite.angers.master.info.api.translater.TranslaterStringToString@6576fe71, request=0, threadCommunication=Thread[Communication0,5,main]]
23:01:18,732 DEBUG Client0:Client:run:130 - Communication ouverte : true
23:01:18,732 DEBUG Client0:Client:run:134 - Communication en cours ...
23:01:18,732 DEBUG Thread-1:ReaderSocketStream:read:30 - Socket : Socket[addr=/127.0.0.1,port=2345,localport=35754]
23:01:18,732 DEBUG Thread-0:ReaderSocketStream:read:30 - Socket : Socket[addr=/127.0.0.1,port=2345,localport=35756]
23:01:18,732 DEBUG Client1:Client:run:130 - Communication ouverte : true
23:01:18,732 DEBUG Client1:Client:run:134 - Communication en cours ...
23:01:18,732 DEBUG Thread-0:ReaderSocketStream:read:39 - Input stream : java.net.SocketInputStream@b4921e1
23:01:18,732 DEBUG Communication0:ClientService:talk:35 - Wait Request ...
23:01:18,732 DEBUG Communication0:ClientService:talk:35 - Wait Request ...
23:01:18,732 DEBUG Thread-1:ReaderSocketStream:read:39 - Input stream : java.net.SocketInputStream@680e9811
23:01:18,732 DEBUG Thread-0:ReaderSocketStream:read:44 - Reader : java.io.BufferedInputStream@58edef8
23:01:18,733 DEBUG Thread-1:ReaderSocketStream:read:44 - Reader : java.io.BufferedInputStream@66c96b60
23:01:18,743 DEBUG Thread-1:ReaderSocketStream:read:52 - Response : HELLO_CLIENT;Welcome client : Socket[addr=/127.0.0.1,port=35754,localport=2345]
23:01:18,744 DEBUG Thread-1:SocketService:run:78 - Message notify : HELLO_CLIENT;Welcome client : Socket[addr=/127.0.0.1,port=35754,localport=2345]
23:01:18,744 DEBUG Thread-1:ConverterStringToString:convert:19 - Message to convert : HELLO_CLIENT;Welcome client : Socket[addr=/127.0.0.1,port=35754,localport=2345]
23:01:18,744 DEBUG Thread-1:SocketService:run:84 - Message translate notify : HELLO_CLIENT;Welcome client : Socket[addr=/127.0.0.1,port=35754,localport=2345]
23:01:18,744 DEBUG Thread-1:Service:isNotify:90 - Message notify : HELLO_CLIENT;Welcome client : Socket[addr=/127.0.0.1,port=35754,localport=2345]
23:01:18,744 DEBUG Thread-1:Service:isNotify:95 - Command name notify : HELLO_CLIENT
23:01:18,744 DEBUG Thread-1:Service:isNotify:102 - Is notify : true
23:01:18,744 DEBUG Thread-1:SocketService:run:92 - Message is notify
23:01:18,744 DEBUG Thread-1:Service:notify:109 - Message notify : HELLO_CLIENT;Welcome client : Socket[addr=/127.0.0.1,port=35754,localport=2345]
23:01:18,744 DEBUG Thread-1:Service:notify:113 - Command name notify : HELLO_CLIENT
23:01:18,744 DEBUG Thread-1:Service:notify:120 - Command : universite.angers.master.info.network.client.command.notify.HelloClientNotify@1e80bfe8
HELLO_CLIENT;Welcome client : Socket[addr=/127.0.0.1,port=35754,localport=2345]
23:01:18,745 DEBUG Thread-1:ReaderSocketStream:read:30 - Socket : Socket[addr=/127.0.0.1,port=2345,localport=35754]
23:01:18,745 DEBUG Thread-1:ReaderSocketStream:read:39 - Input stream : java.net.SocketInputStream@680e9811
23:01:18,745 DEBUG Thread-1:ReaderSocketStream:read:44 - Reader : java.io.BufferedInputStream@62f6f361
23:01:18,751 DEBUG Thread-0:ReaderSocketStream:read:52 - Response : HELLO_CLIENT;Welcome client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,751 DEBUG Thread-0:SocketService:run:78 - Message notify : HELLO_CLIENT;Welcome client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,751 DEBUG Thread-0:ConverterStringToString:convert:19 - Message to convert : HELLO_CLIENT;Welcome client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,751 DEBUG Thread-0:SocketService:run:84 - Message translate notify : HELLO_CLIENT;Welcome client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,751 DEBUG Thread-0:Service:isNotify:90 - Message notify : HELLO_CLIENT;Welcome client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,751 DEBUG Thread-0:Service:isNotify:95 - Command name notify : HELLO_CLIENT
23:01:18,751 DEBUG Thread-0:Service:isNotify:102 - Is notify : true
23:01:18,751 DEBUG Thread-0:SocketService:run:92 - Message is notify
23:01:18,751 DEBUG Thread-0:Service:notify:109 - Message notify : HELLO_CLIENT;Welcome client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,751 DEBUG Thread-0:Service:notify:113 - Command name notify : HELLO_CLIENT
23:01:18,751 DEBUG Thread-0:Service:notify:120 - Command : universite.angers.master.info.network.client.command.notify.HelloClientNotify@65b3120a
HELLO_CLIENT;Welcome client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,751 DEBUG Thread-0:ReaderSocketStream:read:30 - Socket : Socket[addr=/127.0.0.1,port=2345,localport=35756]
23:01:18,751 DEBUG Thread-0:ReaderSocketStream:read:39 - Input stream : java.net.SocketInputStream@b4921e1
23:01:18,751 DEBUG Thread-0:ReaderSocketStream:read:44 - Reader : java.io.BufferedInputStream@1b14a91
23:01:18,751 DEBUG Thread-1:ReaderSocketStream:read:52 - Response : NEW_CLIENT;New client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,752 DEBUG Thread-1:SocketService:run:78 - Message notify : NEW_CLIENT;New client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,752 DEBUG Thread-1:ConverterStringToString:convert:19 - Message to convert : NEW_CLIENT;New client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,752 DEBUG Thread-1:SocketService:run:84 - Message translate notify : NEW_CLIENT;New client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,752 DEBUG Thread-1:Service:isNotify:90 - Message notify : NEW_CLIENT;New client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,754 DEBUG Thread-1:Service:isNotify:95 - Command name notify : NEW_CLIENT
23:01:18,754 DEBUG Thread-1:Service:isNotify:102 - Is notify : true
23:01:18,754 DEBUG Thread-1:SocketService:run:92 - Message is notify
23:01:18,754 DEBUG Thread-1:Service:notify:109 - Message notify : NEW_CLIENT;New client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,754 DEBUG Thread-1:Service:notify:113 - Command name notify : NEW_CLIENT
23:01:18,754 DEBUG Thread-1:Service:notify:120 - Command : universite.angers.master.info.network.client.command.notify.NewClientNotify@66a29884
NEW_CLIENT;New client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,754 DEBUG Thread-1:ReaderSocketStream:read:30 - Socket : Socket[addr=/127.0.0.1,port=2345,localport=35754]
23:01:18,754 DEBUG Thread-1:ReaderSocketStream:read:39 - Input stream : java.net.SocketInputStream@680e9811
23:01:18,754 DEBUG Thread-1:ReaderSocketStream:read:44 - Reader : java.io.BufferedInputStream@27f4615b
23:01:18,765 DEBUG Thread-1:ReaderSocketStream:read:52 - Response : NEW_CLIENT;New client : Socket[addr=/127.0.0.1,port=35758,localport=2345]
23:01:18,765 DEBUG Thread-1:SocketService:run:78 - Message notify : NEW_CLIENT;New client : Socket[addr=/127.0.0.1,port=35758,localport=2345]
23:01:18,765 DEBUG Thread-1:ConverterStringToString:convert:19 - Message to convert : NEW_CLIENT;New client : Socket[addr=/127.0.0.1,port=35758,localport=2345]
23:01:18,765 DEBUG Thread-1:SocketService:run:84 - Message translate notify : NEW_CLIENT;New client : Socket[addr=/127.0.0.1,port=35758,localport=2345]
23:01:18,765 DEBUG Thread-1:Service:isNotify:90 - Message notify : NEW_CLIENT;New client : Socket[addr=/127.0.0.1,port=35758,localport=2345]
23:01:18,765 DEBUG Thread-0:ReaderSocketStream:read:52 - Response : NEW_CLIENT;New client : Socket[addr=/127.0.0.1,port=35758,localport=2345]
23:01:18,765 DEBUG Thread-1:Service:isNotify:95 - Command name notify : NEW_CLIENT
23:01:18,765 DEBUG Thread-1:Service:isNotify:102 - Is notify : true
23:01:18,765 DEBUG Thread-1:SocketService:run:92 - Message is notify
23:01:18,765 DEBUG Thread-1:Service:notify:109 - Message notify : NEW_CLIENT;New client : Socket[addr=/127.0.0.1,port=35758,localport=2345]
23:01:18,765 DEBUG Thread-0:SocketService:run:78 - Message notify : NEW_CLIENT;New client : Socket[addr=/127.0.0.1,port=35758,localport=2345]
23:01:18,765 DEBUG Thread-1:Service:notify:113 - Command name notify : NEW_CLIENT
23:01:18,765 DEBUG Thread-0:ConverterStringToString:convert:19 - Message to convert : NEW_CLIENT;New client : Socket[addr=/127.0.0.1,port=35758,localport=2345]
23:01:18,765 DEBUG Thread-1:Service:notify:120 - Command : universite.angers.master.info.network.client.command.notify.NewClientNotify@66a29884
NEW_CLIENT;New client : Socket[addr=/127.0.0.1,port=35758,localport=2345]
23:01:18,765 DEBUG Thread-0:SocketService:run:84 - Message translate notify : NEW_CLIENT;New client : Socket[addr=/127.0.0.1,port=35758,localport=2345]
23:01:18,765 DEBUG Thread-1:ReaderSocketStream:read:30 - Socket : Socket[addr=/127.0.0.1,port=2345,localport=35754]
23:01:18,765 DEBUG Thread-0:Service:isNotify:90 - Message notify : NEW_CLIENT;New client : Socket[addr=/127.0.0.1,port=35758,localport=2345]
23:01:18,765 DEBUG Thread-1:ReaderSocketStream:read:39 - Input stream : java.net.SocketInputStream@680e9811
23:01:18,765 DEBUG Thread-0:Service:isNotify:95 - Command name notify : NEW_CLIENT
23:01:18,765 DEBUG Thread-1:ReaderSocketStream:read:44 - Reader : java.io.BufferedInputStream@3b26aba4
23:01:18,766 DEBUG Thread-0:Service:isNotify:102 - Is notify : true
23:01:18,766 DEBUG Thread-0:SocketService:run:92 - Message is notify
23:01:18,766 DEBUG Thread-0:Service:notify:109 - Message notify : NEW_CLIENT;New client : Socket[addr=/127.0.0.1,port=35758,localport=2345]
23:01:18,766 DEBUG Thread-0:Service:notify:113 - Command name notify : NEW_CLIENT
23:01:18,766 DEBUG Thread-0:Service:notify:120 - Command : universite.angers.master.info.network.client.command.notify.NewClientNotify@6f539caf
NEW_CLIENT;New client : Socket[addr=/127.0.0.1,port=35758,localport=2345]
23:01:18,766 DEBUG Thread-0:ReaderSocketStream:read:30 - Socket : Socket[addr=/127.0.0.1,port=2345,localport=35756]
23:01:18,766 DEBUG Thread-0:ReaderSocketStream:read:39 - Input stream : java.net.SocketInputStream@b4921e1
23:01:18,766 DEBUG Thread-0:ReaderSocketStream:read:44 - Reader : java.io.BufferedInputStream@44b3d4c3
23:01:18,778 DEBUG Communication0:ClientService:talk:37 - Request : universite.angers.master.info.network.client.command.request.DateObjectYearCommand@50040f0c
23:01:18,778 DEBUG Communication0:Communicator:communicateBetweenEntityAAndB:118 - Entité A talk : YEAR
23:01:18,778 DEBUG Communication0:ConverterStringToString:convert:19 - Message to convert : YEAR
23:01:18,778 DEBUG Communication0:Communicator:communicateBetweenEntityAAndB:126 - Message translate to entity B : YEAR
23:01:18,778 DEBUG Communication0:WriterSocketStream:write:30 - Socket : Socket[addr=/127.0.0.1,port=2345,localport=35756]
23:01:18,778 DEBUG Communication0:WriterSocketStream:write:31 - Message to write : YEAR
23:01:18,778 DEBUG Communication0:WriterSocketStream:write:38 - Output stream : java.net.SocketOutputStream@3a7d7869
23:01:18,778 DEBUG Communication0:WriterSocketStream:write:43 - Writer : java.io.PrintWriter@71eb22b1
23:01:18,779 DEBUG Communication0:Communicator:communicateBetweenEntityAAndB:134 - Entity B understand A : true
23:01:18,779 DEBUG Communication0:Communicator:run:87 - A understand B : true
23:01:18,782 DEBUG Communication0:ClientService:talk:37 - Request : universite.angers.master.info.network.client.command.request.DateObjectMonthCommand@17a7cec2
23:01:18,782 DEBUG Communication0:Communicator:communicateBetweenEntityAAndB:118 - Entité A talk : MONTH
23:01:18,782 DEBUG Communication0:ConverterStringToString:convert:19 - Message to convert : MONTH
23:01:18,782 DEBUG Communication0:Communicator:communicateBetweenEntityAAndB:126 - Message translate to entity B : MONTH
23:01:18,782 DEBUG Communication0:WriterSocketStream:write:30 - Socket : Socket[addr=/127.0.0.1,port=2345,localport=35754]
23:01:18,782 DEBUG Communication0:WriterSocketStream:write:31 - Message to write : MONTH
23:01:18,782 DEBUG Communication0:WriterSocketStream:write:38 - Output stream : java.net.SocketOutputStream@6940f36f
23:01:18,782 DEBUG Communication0:WriterSocketStream:write:43 - Writer : java.io.PrintWriter@2bb83b3d
23:01:18,783 DEBUG Communication0:Communicator:communicateBetweenEntityAAndB:134 - Entity B understand A : true
23:01:18,783 DEBUG Communication0:Communicator:run:87 - A understand B : true
23:01:18,797 DEBUG Thread-1:ReaderSocketStream:read:52 - Response : 28
23:01:18,798 DEBUG Thread-1:SocketService:run:78 - Message notify : 28
23:01:18,798 DEBUG Thread-1:ConverterStringToString:convert:19 - Message to convert : 28
23:01:18,798 DEBUG Thread-1:SocketService:run:84 - Message translate notify : 28
23:01:18,798 DEBUG Thread-1:Service:isNotify:90 - Message notify : 28
23:01:18,798 DEBUG Thread-1:Service:isNotify:95 - Command name notify : 28
23:01:18,798 DEBUG Thread-1:Service:isNotify:102 - Is notify : false
23:01:18,798 DEBUG Thread-1:SocketService:run:95 - Message is not a notify
23:01:18,798 DEBUG Thread-1:ReaderSocketStream:read:30 - Socket : Socket[addr=/127.0.0.1,port=2345,localport=35754]
23:01:18,798 DEBUG Thread-1:ReaderSocketStream:read:39 - Input stream : java.net.SocketInputStream@680e9811
23:01:18,798 DEBUG Thread-1:ReaderSocketStream:read:44 - Reader : java.io.BufferedInputStream@161945cb
23:01:18,799 DEBUG Communication0:Communicator:communicateBetweenEntityBAndA:148 - Entité B talk : 28
23:01:18,799 DEBUG Communication0:ConverterStringToString:convert:19 - Message to convert : 28
23:01:18,799 DEBUG Communication0:Communicator:communicateBetweenEntityBAndA:156 - Message translate to entity B : 28
23:01:18,799 DEBUG Communication0:ClientService:listen:50 - Response : 28
23:01:18,799 DEBUG Thread-0:ReaderSocketStream:read:52 - Response : 59
23:01:18,799 DEBUG Communication0:ClientService:listen:64 - Listen request current : true
23:01:18,799 DEBUG Thread-0:SocketService:run:78 - Message notify : 59
23:01:18,799 DEBUG Communication0:Communicator:communicateBetweenEntityBAndA:164 - Entity A understand B : true
23:01:18,799 DEBUG Thread-0:ConverterStringToString:convert:19 - Message to convert : 59
23:01:18,799 DEBUG Thread-0:SocketService:run:84 - Message translate notify : 59
23:01:18,799 DEBUG Thread-0:Service:isNotify:90 - Message notify : 59
23:01:18,799 DEBUG Thread-0:Service:isNotify:95 - Command name notify : 59
23:01:18,799 DEBUG Thread-0:Service:isNotify:102 - Is notify : false
23:01:18,799 DEBUG Thread-0:SocketService:run:95 - Message is not a notify
23:01:18,799 DEBUG Thread-0:ReaderSocketStream:read:30 - Socket : Socket[addr=/127.0.0.1,port=2345,localport=35756]
23:01:18,799 DEBUG Thread-0:ReaderSocketStream:read:39 - Input stream : java.net.SocketInputStream@b4921e1
23:01:18,799 DEBUG Thread-0:ReaderSocketStream:read:44 - Reader : java.io.BufferedInputStream@6b7a39d
23:01:18,799 DEBUG Communication0:Communicator:run:94 - B understand A : true
23:01:18,800 DEBUG Communication0:Communicator:run:100 - Echanges : 1
23:01:18,800 DEBUG Communication0:Communicator:run:101 - EntityA is open : true
23:01:18,800 DEBUG Communication0:Communicator:run:102 - EntityA is limited : false
23:01:18,800 DEBUG Communication0:Communicator:run:103 - EntityB is open : true
23:01:18,800 DEBUG Communication0:Communicator:run:104 - EntityB is limited : false
23:01:18,800 DEBUG Communication0:ClientService:talk:35 - Wait Request ...
23:01:18,800 DEBUG Communication0:ClientService:talk:37 - Request : universite.angers.master.info.network.client.command.request.DateObjectDayCommand@cc34f4d
23:01:18,800 DEBUG Communication0:Communicator:communicateBetweenEntityAAndB:118 - Entité A talk : DAY
23:01:18,800 DEBUG Communication0:ConverterStringToString:convert:19 - Message to convert : DAY
23:01:18,800 DEBUG Communication0:Communicator:communicateBetweenEntityAAndB:126 - Message translate to entity B : DAY
23:01:18,800 DEBUG Communication0:WriterSocketStream:write:30 - Socket : Socket[addr=/127.0.0.1,port=2345,localport=35754]
23:01:18,800 DEBUG Communication0:WriterSocketStream:write:31 - Message to write : DAY
23:01:18,800 DEBUG Communication0:WriterSocketStream:write:38 - Output stream : java.net.SocketOutputStream@6940f36f
23:01:18,800 DEBUG Communication0:WriterSocketStream:write:43 - Writer : java.io.PrintWriter@6bbd6808
23:01:18,800 DEBUG Communication0:Communicator:communicateBetweenEntityAAndB:134 - Entity B understand A : true
23:01:18,800 DEBUG Communication0:Communicator:run:87 - A understand B : true
23:01:18,799 DEBUG Communication0:Communicator:communicateBetweenEntityBAndA:148 - Entité B talk : 59
23:01:18,800 DEBUG Communication0:ConverterStringToString:convert:19 - Message to convert : 59
23:01:18,800 DEBUG Communication0:Communicator:communicateBetweenEntityBAndA:156 - Message translate to entity B : 59
23:01:18,800 DEBUG Communication0:ClientService:listen:50 - Response : 59
23:01:18,800 DEBUG Communication0:ClientService:listen:64 - Listen request current : true
23:01:18,800 DEBUG Communication0:Communicator:communicateBetweenEntityBAndA:164 - Entity A understand B : true
23:01:18,801 DEBUG Communication0:Communicator:run:94 - B understand A : true
23:01:18,802 DEBUG Thread-1:ReaderSocketStream:read:52 - Response : 6
23:01:18,802 DEBUG Thread-1:SocketService:run:78 - Message notify : 6
23:01:18,802 DEBUG Thread-1:ConverterStringToString:convert:19 - Message to convert : 6
23:01:18,802 DEBUG Thread-1:SocketService:run:84 - Message translate notify : 6
23:01:18,802 DEBUG Thread-1:Service:isNotify:90 - Message notify : 6
23:01:18,802 DEBUG Thread-1:Service:isNotify:95 - Command name notify : 6
23:01:18,802 DEBUG Thread-1:Service:isNotify:102 - Is notify : false
23:01:18,802 DEBUG Thread-1:SocketService:run:95 - Message is not a notify
23:01:18,802 DEBUG Thread-1:ReaderSocketStream:read:30 - Socket : Socket[addr=/127.0.0.1,port=2345,localport=35754]
23:01:18,802 DEBUG Thread-1:ReaderSocketStream:read:39 - Input stream : java.net.SocketInputStream@680e9811
23:01:18,802 DEBUG Thread-1:ReaderSocketStream:read:44 - Reader : java.io.BufferedInputStream@4321d192
23:01:18,802 DEBUG Communication0:Communicator:run:100 - Echanges : 1
23:01:18,802 DEBUG Communication0:Communicator:run:101 - EntityA is open : true
23:01:18,802 DEBUG Communication0:Communicator:run:102 - EntityA is limited : false
23:01:18,802 DEBUG Communication0:Communicator:run:103 - EntityB is open : true
23:01:18,802 DEBUG Communication0:Communicator:run:104 - EntityB is limited : false
23:01:18,803 DEBUG Communication0:ClientService:talk:35 - Wait Request ...
23:01:18,803 DEBUG Communication0:ClientService:talk:37 - Request : universite.angers.master.info.network.client.command.request.DateObjectHourCommand@2dda6444
23:01:18,803 DEBUG Communication0:Communicator:communicateBetweenEntityAAndB:118 - Entité A talk : TIME
23:01:18,803 DEBUG Communication0:ConverterStringToString:convert:19 - Message to convert : TIME
23:01:18,803 DEBUG Communication0:Communicator:communicateBetweenEntityAAndB:126 - Message translate to entity B : TIME
23:01:18,803 DEBUG Communication0:WriterSocketStream:write:30 - Socket : Socket[addr=/127.0.0.1,port=2345,localport=35756]
23:01:18,803 DEBUG Communication0:WriterSocketStream:write:31 - Message to write : TIME
23:01:18,803 DEBUG Communication0:WriterSocketStream:write:38 - Output stream : java.net.SocketOutputStream@3a7d7869
23:01:18,803 DEBUG Communication0:WriterSocketStream:write:43 - Writer : java.io.PrintWriter@48af8413
23:01:18,803 DEBUG Communication0:Communicator:communicateBetweenEntityAAndB:134 - Entity B understand A : true
23:01:18,804 DEBUG Communication0:Communicator:run:87 - A understand B : true
23:01:18,805 DEBUG Communication0:Communicator:communicateBetweenEntityBAndA:148 - Entité B talk : 6
23:01:18,805 DEBUG Communication0:ConverterStringToString:convert:19 - Message to convert : 6
23:01:18,805 DEBUG Communication0:Communicator:communicateBetweenEntityBAndA:156 - Message translate to entity B : 6
23:01:18,805 DEBUG Communication0:ClientService:listen:50 - Response : 6
23:01:18,805 DEBUG Communication0:ClientService:listen:64 - Listen request current : true
23:01:18,805 DEBUG Communication0:Communicator:communicateBetweenEntityBAndA:164 - Entity A understand B : true
23:01:18,805 DEBUG Communication0:Communicator:run:94 - B understand A : true
23:01:18,805 DEBUG Communication0:Communicator:run:100 - Echanges : 2
23:01:18,806 DEBUG Communication0:Communicator:run:101 - EntityA is open : true
23:01:18,806 DEBUG Communication0:Communicator:run:102 - EntityA is limited : false
23:01:18,806 DEBUG Communication0:Communicator:run:103 - EntityB is open : true
23:01:18,806 DEBUG Communication0:Communicator:run:104 - EntityB is limited : false
23:01:18,806 DEBUG Communication0:ClientService:talk:35 - Wait Request ...
23:01:18,812 DEBUG Thread-0:ReaderSocketStream:read:52 - Response : 23
23:01:18,812 DEBUG Thread-0:SocketService:run:78 - Message notify : 23
23:01:18,812 DEBUG Thread-0:ConverterStringToString:convert:19 - Message to convert : 23
23:01:18,812 DEBUG Thread-0:SocketService:run:84 - Message translate notify : 23
23:01:18,812 DEBUG Thread-0:Service:isNotify:90 - Message notify : 23
23:01:18,812 DEBUG Thread-0:Service:isNotify:95 - Command name notify : 23
23:01:18,812 DEBUG Thread-0:Service:isNotify:102 - Is notify : false
23:01:18,812 DEBUG Thread-0:SocketService:run:95 - Message is not a notify
23:01:18,812 DEBUG Thread-0:ReaderSocketStream:read:30 - Socket : Socket[addr=/127.0.0.1,port=2345,localport=35756]
23:01:18,812 DEBUG Communication0:Communicator:communicateBetweenEntityBAndA:148 - Entité B talk : 23
23:01:18,812 DEBUG Thread-0:ReaderSocketStream:read:39 - Input stream : java.net.SocketInputStream@b4921e1
23:01:18,812 DEBUG Thread-0:ReaderSocketStream:read:44 - Reader : java.io.BufferedInputStream@753dd4ad
23:01:18,813 DEBUG Communication0:ConverterStringToString:convert:19 - Message to convert : 23
23:01:18,813 DEBUG Communication0:Communicator:communicateBetweenEntityBAndA:156 - Message translate to entity B : 23
23:01:18,813 DEBUG Communication0:ClientService:listen:50 - Response : 23
23:01:18,813 DEBUG Communication0:ClientService:listen:64 - Listen request current : true
23:01:18,813 DEBUG Communication0:Communicator:communicateBetweenEntityBAndA:164 - Entity A understand B : true
23:01:18,813 DEBUG Communication0:Communicator:run:94 - B understand A : true
23:01:18,813 DEBUG Communication0:Communicator:run:100 - Echanges : 2
23:01:18,813 DEBUG Communication0:Communicator:run:101 - EntityA is open : true
23:01:18,813 DEBUG Communication0:Communicator:run:102 - EntityA is limited : false
23:01:18,813 DEBUG Communication0:Communicator:run:103 - EntityB is open : true
23:01:18,813 DEBUG Communication0:Communicator:run:104 - EntityB is limited : false
23:01:18,813 DEBUG Communication0:ClientService:talk:35 - Wait Request ...
23:01:18,813 DEBUG main:Client:close:212 - Wait requests done
23:01:18,813 DEBUG main:Client:close:214 - Requests done
23:01:18,813 DEBUG main:Client:notifyObserver:149 - Subject notify : CLOSE_CLIENT
23:01:18,814 DEBUG main:Client:notifyObserver:159 - Notification : universite.angers.master.info.network.client.command.notify.CloseClientNotify@79fc0f2f
23:01:18,814 DEBUG main:Client:notifyObserver:164 - Message notify : CLOSE_CLIENT;35756
23:01:18,814 DEBUG main:ConverterStringToString:convert:19 - Message to convert : CLOSE_CLIENT;35756
23:01:18,814 DEBUG main:Communicator:notifyAtoB:202 - Message translate to entity B : CLOSE_CLIENT;35756
23:01:18,814 DEBUG main:WriterSocketStream:write:30 - Socket : Socket[addr=/127.0.0.1,port=2345,localport=35756]
23:01:18,814 DEBUG main:WriterSocketStream:write:31 - Message to write : CLOSE_CLIENT;35756
23:01:18,814 DEBUG main:WriterSocketStream:write:38 - Output stream : java.net.SocketOutputStream@3a7d7869
23:01:18,814 DEBUG main:WriterSocketStream:write:43 - Writer : java.io.PrintWriter@4d405ef7
23:01:18,815 DEBUG main:Client:close:221 - Service client close : true
23:01:18,815 DEBUG Communication0:ClientService:talk:37 - Request : null
23:01:18,815 DEBUG Communication0:Communicator:communicateBetweenEntityAAndB:118 - Entité A talk : null
23:01:18,815 DEBUG Communication0:Communicator:run:87 - A understand B : false
23:01:18,815 DEBUG Client1:Client:run:136 - Communication terminée
23:01:18,815 DEBUG Thread-0:SocketService:run:78 - Message notify : null
23:01:18,815 DEBUG main:Client:close:225 - Communication fermée : true
23:01:18,815 DEBUG main:Client:close:236 - Client fermée : 
23:01:18,842 DEBUG Thread-1:ReaderSocketStream:read:52 - Response : CLOSE_CLIENT;Close client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,842 DEBUG Thread-1:SocketService:run:78 - Message notify : CLOSE_CLIENT;Close client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,842 DEBUG Thread-1:ConverterStringToString:convert:19 - Message to convert : CLOSE_CLIENT;Close client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,842 DEBUG Thread-1:SocketService:run:84 - Message translate notify : CLOSE_CLIENT;Close client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,842 DEBUG Thread-1:Service:isNotify:90 - Message notify : CLOSE_CLIENT;Close client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,842 DEBUG Thread-1:Service:isNotify:95 - Command name notify : CLOSE_CLIENT
23:01:18,842 DEBUG Thread-1:Service:isNotify:102 - Is notify : true
23:01:18,842 DEBUG Thread-1:SocketService:run:92 - Message is notify
23:01:18,842 DEBUG Thread-1:Service:notify:109 - Message notify : CLOSE_CLIENT;Close client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,843 DEBUG Thread-1:Service:notify:113 - Command name notify : CLOSE_CLIENT
23:01:18,843 DEBUG Thread-1:Service:notify:120 - Command : universite.angers.master.info.network.client.command.notify.CloseClientNotify@4769b07b
CLOSE_CLIENT;Close client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:18,843 DEBUG Thread-1:ReaderSocketStream:read:30 - Socket : Socket[addr=/127.0.0.1,port=2345,localport=35754]
23:01:18,843 DEBUG Thread-1:ReaderSocketStream:read:39 - Input stream : java.net.SocketInputStream@680e9811
23:01:18,843 DEBUG Thread-1:ReaderSocketStream:read:44 - Reader : java.io.BufferedInputStream@47db9e14
23:01:19,816 DEBUG main:Client:open:187 - Open client service : true
23:01:19,816 DEBUG main:Client:close:212 - Wait requests done
23:01:19,816 DEBUG main:Client:close:214 - Requests done
23:01:19,817 DEBUG Client2:ReaderSocket:<init>:29 - Socket : null
23:01:19,817 DEBUG Client2:Client:run:113 - Reader socket : ReaderSocketStream [socket=null]
23:01:19,817 DEBUG Client2:Client:run:117 - Writer socket : WriterSocketStream [socket=null]
23:01:19,817 DEBUG main:Client:notifyObserver:149 - Subject notify : CLOSE_CLIENT
23:01:19,818 DEBUG main:Client:notifyObserver:159 - Notification : universite.angers.master.info.network.client.command.notify.CloseClientNotify@4769b07b
23:01:19,817 DEBUG Client2:Client:run:121 - Service socket : CommunicatorSocket [socket=Socket[addr=/127.0.0.1,port=2345,localport=35758], reader=ReaderSocketStream [socket=Socket[addr=/127.0.0.1,port=2345,localport=35758]], writer=WriterSocketStream [socket=Socket[addr=/127.0.0.1,port=2345,localport=35758]]]
23:01:19,818 DEBUG main:Client:notifyObserver:164 - Message notify : CLOSE_CLIENT;35754
23:01:19,818 DEBUG Client2:Communicator:<init>:54 - Entité A : ClientService [requests=BlockingMap [map={}, finish=false, open=true], requestDefault=null, requestCurrent=null, notifications=BlockingMap [map={HELLO_CLIENT=universite.angers.master.info.network.client.command.notify.HelloClientNotify@5e9f23b4, NEW_CLIENT=universite.angers.master.info.network.client.command.notify.NewClientNotify@4783da3f, CLOSE_CLIENT=universite.angers.master.info.network.client.command.notify.CloseClientNotify@378fd1ac}, finish=false, open=true], notificationDefault=null, commandName=universite.angers.master.info.network.client.command.ConverterCommandNameClient@4edde6e5, lock=universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock@70177ecd]
23:01:19,818 DEBUG main:ConverterStringToString:convert:19 - Message to convert : CLOSE_CLIENT;35754
23:01:19,819 DEBUG Client2:Communicator:<init>:57 - Entité B : CommunicatorSocket [socket=Socket[addr=/127.0.0.1,port=2345,localport=35758], reader=ReaderSocketStream [socket=Socket[addr=/127.0.0.1,port=2345,localport=35758]], writer=WriterSocketStream [socket=Socket[addr=/127.0.0.1,port=2345,localport=35758]]]
23:01:19,819 DEBUG main:Communicator:notifyAtoB:202 - Message translate to entity B : CLOSE_CLIENT;35754
23:01:19,819 DEBUG Client2:Communicator:<init>:60 - Traducteur : universite.angers.master.info.api.translater.TranslaterStringToString@300ffa5d
23:01:19,819 DEBUG main:WriterSocketStream:write:30 - Socket : Socket[addr=/127.0.0.1,port=2345,localport=35754]
23:01:19,819 DEBUG Client2:Communicator:<init>:63 - Echanges : 0
23:01:19,820 DEBUG Client2:Communicator:<init>:66 - Thread communication : Thread[Communication2,5,main]
23:01:19,819 DEBUG main:WriterSocketStream:write:31 - Message to write : CLOSE_CLIENT;35754
23:01:19,820 DEBUG Client2:Client:run:126 - Communication entre client et socket : Communicator [entityA=ClientService [requests=BlockingMap [map={}, finish=false, open=true], requestDefault=null, requestCurrent=null, notifications=BlockingMap [map={HELLO_CLIENT=universite.angers.master.info.network.client.command.notify.HelloClientNotify@5e9f23b4, NEW_CLIENT=universite.angers.master.info.network.client.command.notify.NewClientNotify@4783da3f, CLOSE_CLIENT=universite.angers.master.info.network.client.command.notify.CloseClientNotify@378fd1ac}, finish=false, open=true], notificationDefault=null, commandName=universite.angers.master.info.network.client.command.ConverterCommandNameClient@4edde6e5, lock=universite.angers.master.info.network.lock.ReentrantWithoutReadWriteLock@70177ecd], entityB=CommunicatorSocket [socket=Socket[addr=/127.0.0.1,port=2345,localport=35758], reader=ReaderSocketStream [socket=Socket[addr=/127.0.0.1,port=2345,localport=35758]], writer=WriterSocketStream [socket=Socket[addr=/127.0.0.1,port=2345,localport=35758]]], translator=universite.angers.master.info.api.translater.TranslaterStringToString@300ffa5d, request=0, threadCommunication=Thread[Communication2,5,main]]
23:01:19,820 DEBUG main:WriterSocketStream:write:38 - Output stream : java.net.SocketOutputStream@6940f36f
23:01:19,821 DEBUG main:WriterSocketStream:write:43 - Writer : java.io.PrintWriter@6193b845
23:01:19,822 DEBUG Thread-2:ReaderSocketStream:read:30 - Socket : Socket[addr=/127.0.0.1,port=2345,localport=35758]
23:01:19,822 DEBUG Thread-2:ReaderSocketStream:read:39 - Input stream : java.net.SocketInputStream@3578f524
23:01:19,823 DEBUG Thread-2:ReaderSocketStream:read:44 - Reader : java.io.BufferedInputStream@3bbf1dfb
23:01:19,823 DEBUG Thread-2:ReaderSocketStream:read:52 - Response : HELLO_CLIENT;Welcome client : Socket[addr=/127.0.0.1,port=35758,localport=2345]CLOSE_CLIENT;Close client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:19,823 DEBUG Thread-2:SocketService:run:78 - Message notify : HELLO_CLIENT;Welcome client : Socket[addr=/127.0.0.1,port=35758,localport=2345]CLOSE_CLIENT;Close client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:19,823 DEBUG Thread-2:ConverterStringToString:convert:19 - Message to convert : HELLO_CLIENT;Welcome client : Socket[addr=/127.0.0.1,port=35758,localport=2345]CLOSE_CLIENT;Close client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:19,824 DEBUG Thread-2:SocketService:run:84 - Message translate notify : HELLO_CLIENT;Welcome client : Socket[addr=/127.0.0.1,port=35758,localport=2345]CLOSE_CLIENT;Close client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:19,824 DEBUG Thread-2:Service:isNotify:90 - Message notify : HELLO_CLIENT;Welcome client : Socket[addr=/127.0.0.1,port=35758,localport=2345]CLOSE_CLIENT;Close client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:19,824 DEBUG Thread-2:Service:isNotify:95 - Command name notify : HELLO_CLIENT
23:01:19,824 DEBUG Thread-2:Service:isNotify:102 - Is notify : true
23:01:19,825 DEBUG Thread-2:SocketService:run:92 - Message is notify
23:01:19,825 DEBUG Thread-2:Service:notify:109 - Message notify : HELLO_CLIENT;Welcome client : Socket[addr=/127.0.0.1,port=35758,localport=2345]CLOSE_CLIENT;Close client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:19,825 DEBUG Thread-2:Service:notify:113 - Command name notify : HELLO_CLIENT
23:01:19,826 DEBUG Thread-2:Service:notify:120 - Command : universite.angers.master.info.network.client.command.notify.HelloClientNotify@5e9f23b4
23:01:19,826 DEBUG Communication0:ClientService:talk:37 - Request : null
23:01:19,826 DEBUG Communication0:Communicator:communicateBetweenEntityAAndB:118 - Entité A talk : null
23:01:19,826 DEBUG Communication0:Communicator:run:87 - A understand B : false
23:01:19,826 DEBUG Client0:Client:run:136 - Communication terminée
HELLO_CLIENT;Welcome client : Socket[addr=/127.0.0.1,port=35758,localport=2345]CLOSE_CLIENT;Close client : Socket[addr=/127.0.0.1,port=35756,localport=2345]
23:01:19,827 DEBUG Thread-2:ReaderSocketStream:read:30 - Socket : Socket[addr=/127.0.0.1,port=2345,localport=35758]
23:01:19,827 DEBUG Communication2:ClientService:talk:35 - Wait Request ...
23:01:19,826 DEBUG main:Client:close:221 - Service client close : true
23:01:19,826 DEBUG Thread-1:SocketService:run:78 - Message notify : null
23:01:19,827 DEBUG Thread-2:ReaderSocketStream:read:39 - Input stream : java.net.SocketInputStream@3578f524
23:01:19,828 DEBUG Thread-2:ReaderSocketStream:read:44 - Reader : java.io.BufferedInputStream@3f809e54
23:01:19,828 DEBUG Client2:Client:run:130 - Communication ouverte : true
23:01:19,828 DEBUG main:Client:close:225 - Communication fermée : true
23:01:19,829 DEBUG main:Client:close:236 - Client fermée : 
23:01:19,829 DEBUG main:Client:close:212 - Wait requests done
23:01:19,829 DEBUG main:Client:close:214 - Requests done
23:01:19,829 DEBUG main:Client:notifyObserver:149 - Subject notify : CLOSE_CLIENT
23:01:19,829 DEBUG main:Client:notifyObserver:159 - Notification : universite.angers.master.info.network.client.command.notify.CloseClientNotify@378fd1ac
23:01:19,829 DEBUG main:Client:notifyObserver:164 - Message notify : CLOSE_CLIENT;35758
23:01:19,830 DEBUG main:ConverterStringToString:convert:19 - Message to convert : CLOSE_CLIENT;35758
23:01:19,830 DEBUG main:Communicator:notifyAtoB:202 - Message translate to entity B : CLOSE_CLIENT;35758
23:01:19,830 DEBUG main:WriterSocketStream:write:30 - Socket : Socket[addr=/127.0.0.1,port=2345,localport=35758]
23:01:19,830 DEBUG main:WriterSocketStream:write:31 - Message to write : CLOSE_CLIENT;35758
23:01:19,828 DEBUG Thread-2:ReaderSocketStream:read:52 - Response : CLOSE_CLIENT;Close client : Socket[addr=/127.0.0.1,port=35754,localport=2345]
23:01:19,830 DEBUG Thread-2:SocketService:run:78 - Message notify : CLOSE_CLIENT;Close client : Socket[addr=/127.0.0.1,port=35754,localport=2345]
23:01:19,831 DEBUG Thread-2:ConverterStringToString:convert:19 - Message to convert : CLOSE_CLIENT;Close client : Socket[addr=/127.0.0.1,port=35754,localport=2345]
23:01:19,831 DEBUG Thread-2:SocketService:run:84 - Message translate notify : CLOSE_CLIENT;Close client : Socket[addr=/127.0.0.1,port=35754,localport=2345]
23:01:19,831 DEBUG Thread-2:Service:isNotify:90 - Message notify : CLOSE_CLIENT;Close client : Socket[addr=/127.0.0.1,port=35754,localport=2345]
23:01:19,830 DEBUG main:WriterSocketStream:write:38 - Output stream : java.net.SocketOutputStream@2e817b38
23:01:19,830 DEBUG Client2:Client:run:134 - Communication en cours ...
23:01:19,831 DEBUG main:WriterSocketStream:write:43 - Writer : java.io.PrintWriter@c4437c4
23:01:19,831 DEBUG Thread-2:Service:isNotify:95 - Command name notify : CLOSE_CLIENT
23:01:19,832 DEBUG Thread-2:Service:isNotify:102 - Is notify : true
23:01:19,832 DEBUG Thread-2:SocketService:run:92 - Message is notify
23:01:19,832 DEBUG Thread-2:Service:notify:109 - Message notify : CLOSE_CLIENT;Close client : Socket[addr=/127.0.0.1,port=35754,localport=2345]
23:01:19,832 DEBUG Thread-2:Service:notify:113 - Command name notify : CLOSE_CLIENT
23:01:19,833 DEBUG Thread-2:Service:notify:120 - Command : universite.angers.master.info.network.client.command.notify.CloseClientNotify@378fd1ac
CLOSE_CLIENT;Close client : Socket[addr=/127.0.0.1,port=35754,localport=2345]
23:01:19,833 DEBUG Thread-2:ReaderSocketStream:read:30 - Socket : Socket[addr=/127.0.0.1,port=2345,localport=35758]
23:01:19,833 DEBUG Thread-2:ReaderSocketStream:read:39 - Input stream : java.net.SocketInputStream@3578f524
23:01:19,834 DEBUG Thread-2:ReaderSocketStream:read:44 - Reader : java.io.BufferedInputStream@7a8217c0
23:01:19,832 DEBUG main:Client:close:221 - Service client close : true
23:01:19,836 DEBUG main:Client:close:225 - Communication fermée : true
23:01:19,836 DEBUG main:Client:close:236 - Client fermée : 
Date 1 après : DateObject [day=6, month=28, year=59, hour=23]
Date 2 après : DateObject [day=, month=, year=, hour=]
23:01:19,836 DEBUG Communication2:ClientService:talk:37 - Request : null
23:01:19,836 DEBUG Communication2:Communicator:communicateBetweenEntityAAndB:118 - Entité A talk : null
23:01:19,836 DEBUG Communication2:Communicator:run:87 - A understand B : false
23:01:19,837 DEBUG Client2:Client:run:136 - Communication terminée
23:01:19,848 ERROR Thread-2:ReaderSocketStream:read:57 - Socket closed
java.net.SocketException: Socket closed
	at java.net.SocketInputStream.read(SocketInputStream.java:204)
	at java.net.SocketInputStream.read(SocketInputStream.java:141)
	at java.io.BufferedInputStream.fill(BufferedInputStream.java:246)
	at java.io.BufferedInputStream.read1(BufferedInputStream.java:286)
	at java.io.BufferedInputStream.read(BufferedInputStream.java:345)
	at java.io.FilterInputStream.read(FilterInputStream.java:107)
	at universite.angers.master.info.network.socket.reader.ReaderSocketStream.read(ReaderSocketStream.java:47)
	at universite.angers.master.info.network.socket.reader.ReaderSocketStream.read(ReaderSocketStream.java:1)
	at universite.angers.master.info.network.socket.SocketService.run(SocketService.java:77)
	at java.lang.Thread.run(Thread.java:748)
23:01:19,852 DEBUG Thread-2:SocketService:run:78 - Message notify : null
 */
