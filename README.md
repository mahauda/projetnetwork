# Projet Network

Réalisé par :
- Théo MAHAUDA
- Anas TAGUENITI
- Mohamed OUHIRRA

Date : 09/04/2020\
Organisation : Master Informatique M1 à l'université d'Angers\
Version : 1.0\
Référence projet GitLab : https://gitlab.com/mahauda/projetnetwork

## Objectif

Permettre de simplifier la communication entre client / serveur via un mécanisme de requêtes